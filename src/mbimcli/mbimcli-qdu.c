/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * mbimcli -- Command line interface to control MBIM devices
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2018 Google LLC
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include <glib.h>
#include <gio/gio.h>

#include <libmbim-glib.h>
#include "mbim-qdu.h"
#include "mbimcli.h"
#include "mbimcli-helpers.h"

#include "mbim-message-private.h"
#include "mbim-enum-types.h"
#include "mbim-error-types.h"
#include "mbim-device.h"
#include "mbim-utils.h"

/* Context */
typedef struct {
    MbimDevice *device;
    GCancellable *cancellable;
} Context;
static Context *ctx;

typedef struct {
    guint8 sendingData[262144]; 
} BlockData;
static BlockData *blkdata;

/* Options */
static gchar    *set_update_session_str;
static gchar    *ota_file_open_str;
static gchar    *ota_file_write_str;
static gchar    *ota_file_update_str;
static gchar    ota_file_path[200];
static guint32  total_sending_numbers = 0;
static guint32  total_fragment_numbers = 0;
static guint32  out_max_transfer_size;
static guint32  out_max_window_size;
static guint32  ota_index = 0;
static guint8   *data_buffer;
static guint32  upgrade_percent = 0;

//add by sunny 2020 11.12
static FILE *p = NULL;
static GOptionEntry entries[] = {
    { "qdu-update-session", 0, 0, G_OPTION_ARG_STRING, &set_update_session_str,
      "QDU update firmware session",
      "[(MbimQDUSessionAction),(MbimQDUSessionType)]"
    },
    { "qdu-file-open", 0, 0, G_OPTION_ARG_STRING, &ota_file_open_str,
      "QDU firmware file open",
      "[(OTAFilePath)]"
    },
    { "qdu-file-write", 0, 0, G_OPTION_ARG_STRING, &ota_file_write_str,
      "QDU firmware file write",
      "[(OTAFilePath)]"
    },
    { "qdu-ota-update", 0, 0, G_OPTION_ARG_STRING, &ota_file_update_str,
      "QDU firmware file update",
      "[(OTAFilePath)]"
    },
    { NULL }
};

GOptionGroup *
mbimcli_qdu_get_option_group (void)
{
   GOptionGroup *group;

   group = g_option_group_new ("qdu",
                               "QDU Service options:",
                               "Show QDU Service options",
                               NULL,
                               NULL);
   g_option_group_add_entries (group, entries);

   return group;
}

gboolean
mbimcli_qdu_options_enabled (void)
{
    static guint n_actions = 0;
    static gboolean checked = FALSE;

    if (checked)
        return !!n_actions;

    n_actions = (!!set_update_session_str +
                 !!ota_file_open_str      +
                 !!ota_file_write_str     +
                 !!ota_file_update_str);

    if (n_actions > 1) {
        g_printerr ("error: too many QDU Service actions requested\n");
        exit (EXIT_FAILURE);
    }

    checked = TRUE;
    return !!n_actions;
}

static void
context_free (Context *context)
{
    if (!context)
        return;

    if (context->cancellable)
        g_object_unref (context->cancellable);
    if (context->device)
        g_object_unref (context->device);
    g_slice_free (Context, context);
}

static void
shutdown (gboolean operation_status)
{
    /* Cleanup context and finish async operation */
    context_free (ctx);
    mbimcli_async_operation_done (operation_status);
}

static void
qdu_update_session_ready (MbimDevice   *device,
                          GAsyncResult *res)
{
    g_autoptr(MbimMessage) response = NULL;
    MbimQDUSessionType     out_current_session_type;
    MbimQDUSessionStatus   out_current_session_status;
    MbimQDUSessionType     out_last_session_type;
    MbimQDUSessionResult   out_last_session_result;
    guint32                out_last_session_error_offset;
    guint32                out_last_session_error_size;
    g_autoptr(GError)      error = NULL;
   
    response = mbim_device_command_finish (device, res, &error);
    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }

    if (!mbim_message_qdu_update_session_response_parse (
          response,
          &out_current_session_type,
          &out_current_session_status,
          &out_last_session_type,
          &out_last_session_result,
          &out_last_session_error_offset,
          &out_last_session_error_size,
          &error)) {
        g_printerr ("error: couldn't parse response message: %s\n", error->message);
        g_printerr ("out_current_session_type: %d\n", (int)out_current_session_type);
        g_printerr ("out_current_session_status: %d\n", (int)out_current_session_status);
        g_printerr ("out_last_session_type: %d\n", (int)out_last_session_type);
        g_printerr ("out_last_session_result: %d\n", (int)out_last_session_result);
        g_printerr ("out_last_session_error_offset: %d\n", (int)out_last_session_error_offset);
        g_printerr ("out_last_session_error_size: %d\n", (int)out_last_session_error_size);
        shutdown (FALSE);
        return;
    }

    g_printerr ("out_current_session_type: %d\n", (int)out_current_session_type);
    g_printerr ("out_current_session_status: %d\n", (int)out_current_session_status);
    g_printerr ("out_last_session_type: %d\n", (int)out_last_session_type);
    g_printerr ("out_last_session_result: %d\n", (int)out_last_session_result);  
    g_print ("[%s] Successfully requested modem to update session\n\n", mbim_device_get_path_display (device));

    shutdown (TRUE);
}

static void
fx_qdu_file_write_ready (MbimDevice   *device,
                         GAsyncResult *res)
{
    g_autoptr(MbimMessage) request  = NULL;
    g_autoptr(MbimMessage) response = NULL;
    g_autoptr(GError)      error = NULL;
    size_t result;

    response = mbim_device_command_finish (device, res, &error);
    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }
    
    if (!mbim_message_qdu_file_write_response_parse (
          response,
          &error)) 
    {
        g_printerr ("[%s][%d]: error: couldn't parse response message: %s\n", __FUNCTION__, __LINE__, error->message);
        shutdown (FALSE);
        return;
    }

    if(ota_index != total_sending_numbers) 
    {
		blkdata = malloc(sizeof(BlockData));
        result = fread(blkdata->sendingData, sizeof(guint8), sizeof(guint8)*262144, p);
        if(result != sizeof(BlockData)) {//check read size is not the same
            g_print("[%s][%d]:read error , result = %ld\n", __FUNCTION__, __LINE__, result);   
        }
        request = (mbim_message_qdu_file_write_set_new (262144, (const guint8 *)(blkdata->sendingData), &error));

        ota_index ++;

        //update percentage display
        if(upgrade_percent != (ota_index*100 / total_sending_numbers))
        {
            upgrade_percent = (ota_index*100 / total_sending_numbers);
            g_print("\b\b\b\b%3d%%", upgrade_percent);
        }
     
        free(blkdata);
        blkdata = NULL; 
        mbim_device_command (ctx->device,
                             request,
                             30,
                             ctx->cancellable,
                             (GAsyncReadyCallback)fx_qdu_file_write_ready,
                             NULL);
    }
    else
    {
        g_print("\n");
        g_print("Successfully upgrade OTA image\n");
        g_print("Wait switch bootup partition and reboot\n");
        sleep(1);
        fclose(p);
        shutdown (TRUE);
    }

}

static void
fx_qdu_file_open_ready (MbimDevice   *device,
                        GAsyncResult *res)
{
    g_autoptr(MbimMessage) request = NULL;
    g_autoptr(MbimMessage) response = NULL;
    g_autoptr(GError)      error = NULL;
    struct stat st;
    size_t result;

    response = mbim_device_command_finish (device, res, &error);

    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }
    
    g_print("[%s] Successfully opening QDU file\n" , mbim_device_get_path_display(device));

    if (!mbim_message_qdu_file_open_response_parse (
          response,
          &out_max_transfer_size,
          &out_max_window_size,
          &error)) 
    {
        g_printerr ("error: couldn't parse response message: %s\n", error->message);
        g_printerr ("out_max_transfer_size: %d\n", (int)out_max_transfer_size);
        g_printerr ("out_max_window_size: %d\n", (int)out_max_window_size);
        shutdown (FALSE);
        return;
    }
    
    stat(ota_file_path, &st);

    p = fopen(ota_file_path, "r");
    if(p == NULL)
    {
        g_print("[%s][%d]: OTA file doesn't exist!\n", __FUNCTION__, __LINE__);
        return;
    }

    total_sending_numbers = (st.st_size) / (out_max_transfer_size); //262144
    if((st.st_size) % (out_max_transfer_size) != 0)
        total_sending_numbers ++;
    total_fragment_numbers = (out_max_transfer_size / 4096 + 1); //+1 total list fragment packet
    
 
    blkdata = malloc(sizeof(BlockData));

    result = fread(blkdata->sendingData, sizeof(guint8), sizeof(guint8)*262144, p);
    if(result != sizeof(BlockData)) //check read size is not the same
          g_print("[%s][%d]:read error , result = %ld\n", __FUNCTION__, __LINE__, result);

    request = (mbim_message_qdu_file_write_set_new (262144, (const guint8 *)(blkdata->sendingData), &error));
    if(ota_index == 0)
    {
        g_print("========> Update OTA file: %3d%%", upgrade_percent);
    }
    
    ota_index ++;
    mbim_device_command (ctx->device,
                         request,
                         10,
                         ctx->cancellable,
                         (GAsyncReadyCallback)fx_qdu_file_write_ready,
                         NULL); 

}

static void
fx_qdu_update_session_ready (MbimDevice   *device,
                             GAsyncResult *res)
{
    g_autoptr(MbimMessage) response = NULL;
    MbimQDUSessionType     out_current_session_type;
    MbimQDUSessionStatus   out_current_session_status;
    MbimQDUSessionType     out_last_session_type;
    MbimQDUSessionResult   out_last_session_result;
    guint32                out_last_session_error_offset;
    guint32                out_last_session_error_size;
    g_autoptr(GError)      error = NULL;
    g_autoptr(MbimMessage) request = NULL;
    struct stat            st;

    response = mbim_device_command_finish (device, res, &error);
    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }

    if (!mbim_message_qdu_update_session_response_parse (
          response,
          &out_current_session_type,
          &out_current_session_status,
          &out_last_session_type,
          &out_last_session_result,
          &out_last_session_error_offset,
          &out_last_session_error_size,
          &error)) 
    {
        g_printerr ("error: couldn't parse response message: %s\n", error->message);
        g_printerr ("out_current_session_type: %d\n", (int)out_current_session_type);
        g_printerr ("out_current_session_status: %d\n", (int)out_current_session_status);
        g_printerr ("out_last_session_type: %d\n", (int)out_last_session_type);
        g_printerr ("out_last_session_result: %d\n", (int)out_last_session_result);
        g_printerr ("out_last_session_error_offset: %d\n", (int)out_last_session_error_offset);
        g_printerr ("out_last_session_error_size: %d\n", (int)out_last_session_error_size);
        
        shutdown (FALSE);
        return;
    }

    g_print ("[%s] Successfully requested modem to update session\n\n", mbim_device_get_path_display (device));
    g_print("Asynchronously opening QDU file... %s\n", ota_file_path);

    stat(ota_file_path, &st);
    
    //send the file open request
	request = (mbim_message_qdu_file_open_set_new (0, st.st_size, &error));
    mbim_device_command (ctx->device,
                         request,
                         10,
                         ctx->cancellable,
                         (GAsyncReadyCallback)fx_qdu_file_open_ready,
                         NULL);

}

static void
qdu_file_open_ready (MbimDevice   *device,
                     GAsyncResult *res)
{
    g_autoptr(MbimMessage) response = NULL;
    g_autoptr(GError)      error = NULL;

    response = mbim_device_command_finish (device, res, &error);

    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }

    if (!mbim_message_qdu_file_open_response_parse (
          response,
          &out_max_transfer_size,
          &out_max_window_size,
          &error)) {
        g_printerr ("error: couldn't parse response message: %s\n", error->message);
        g_printerr ("out_max_transfer_size: %d\n", (int)out_max_transfer_size);
        g_printerr ("out_max_window_size: %d\n", (int)out_max_window_size);
        shutdown (FALSE);
        return;
    }

    g_print ("[%s] Successfully requested qdu file open\n\n", mbim_device_get_path_display (device));

    shutdown (TRUE);
}

static void
qdu_file_write_ready (MbimDevice   *device,
                      GAsyncResult *res)
{
    g_autoptr(MbimMessage) response = NULL;
    g_autoptr(GError)      error = NULL;

    response = mbim_device_command_finish (device, res, &error);
    if (!response || !mbim_message_response_get_result (response, MBIM_MESSAGE_TYPE_COMMAND_DONE, &error)) {
        g_printerr ("error: operation failed: %s\n", error->message);
        shutdown (FALSE);
        return;
    }

    g_print ("[%s] Successfully requested qdu file write\n\n", mbim_device_get_path_display (device));

    shutdown (TRUE);
}

static gboolean
update_session_command_parse (const gchar      *str,
                              guint32          *sessionAction,
                              guint32          *sessionType)
{
    g_auto(GStrv) split = NULL;
    gboolean      status = FALSE;

    g_assert (sessionAction != NULL);
    g_assert (sessionType != NULL);

    /* Format of the string is:
     * [(SessionAction),(SessionType)]
     */
    split = g_strsplit (str, ",", -1);

    if (g_strv_length (split) > 2)
        g_printerr ("error: couldn't parse input string, too many arguments\n");
    else if (g_strv_length (split) < 1)
        g_printerr ("error: couldn't parse input string, missing arguments\n");
    else if (!mbimcli_read_uint_from_string (split[0], sessionAction))
        g_printerr ("error: couldn't parse Session Action, should be a number\n");
    else if (!mbimcli_read_uint_from_string (split[1], sessionType))
        g_printerr ("error: couldn't parse Session Type, should be a number\n");
    else
        status = TRUE;

    return status;
}

void
mbimcli_qdu_run (MbimDevice   *device,
                 GCancellable *cancellable)
{
    g_autoptr(MbimMessage) request = NULL;
    g_autoptr(GError) error = NULL;
    guint32 session_action;
    guint32 session_type;
    struct stat st;
    guint32 data_buffer_size = 16;

    /* Initialize context */
    ctx = g_slice_new (Context);
    ctx->device = g_object_ref (device);
    ctx->cancellable = cancellable ? g_object_ref (cancellable) : NULL;

    //////////////////////////////////////////////
    // QDU: update session
    //////////////////////////////////////////////
    if (set_update_session_str) 
    {
        g_debug ("Asynchronously update QDU session...");       

        if (!update_session_command_parse (set_update_session_str, &session_action, &session_type)) {
            shutdown (FALSE);
            return;
        }
        //g_print("[%s][%d]: session_action=%d, session_type=%d\n", __FUNCTION__, __LINE__ , session_action, session_type);

        //send the update session request
		request = (mbim_message_qdu_update_session_set_new (session_action, session_type, &error));

        mbim_device_command (ctx->device,
                             request,
                             10,
                             ctx->cancellable,
                             (GAsyncReadyCallback)qdu_update_session_ready,
                             NULL);
        return;
    }

    //////////////////////////////////////////////
    // QDU: file open
    //////////////////////////////////////////////
    if (ota_file_open_str) 
    {
        g_debug ("Asynchronously opening QDU file... %s", ota_file_open_str);

        stat(ota_file_open_str, &st);
        //g_print("[%s][%d]: (3-2) size = %ld\n", __FUNCTION__, __LINE__, st.st_size);
        
        //send the file open request
		request = (mbim_message_qdu_file_open_set_new (0, st.st_size, &error));
        mbim_device_command (ctx->device,
                             request,
                             10,
                             ctx->cancellable,
                             (GAsyncReadyCallback)qdu_file_open_ready,
                             NULL);

        return;
    }

    //////////////////////////////////////////////
    // QDU: file write
    //////////////////////////////////////////////
    if (ota_file_write_str) 
    {
        g_debug ("Asynchronously writing QDU file...");

        //send the file write request
        data_buffer = malloc(sizeof(guint8)*data_buffer_size);
		request = (mbim_message_qdu_file_write_set_new (data_buffer_size, data_buffer, &error));
        mbim_device_command (ctx->device,
                             request,
                             10,
                             ctx->cancellable,
                             (GAsyncReadyCallback)qdu_file_write_ready,
                             NULL);
        return;
    }

    //////////////////////////////////////////////
    // QDU: ota file update
    //////////////////////////////////////////////
    if (ota_file_update_str) 
    {
        g_print("Asynchronously update QDU ota file...%s\n", ota_file_update_str);
        strcpy(ota_file_path, ota_file_update_str);

		request = (mbim_message_qdu_update_session_set_new (0, 1, &error));
		
        mbim_device_command (ctx->device,
                             request,
                             10,
                             ctx->cancellable,
                             (GAsyncReadyCallback)fx_qdu_update_session_ready,
                             NULL);      
        return;
    }   
    
    g_warn_if_reached (); //log a message
}

